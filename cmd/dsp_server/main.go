package main

import (
	dspserver "gitlab.com/rapidcodelab-opensource/smartyads-native-wrapper/internal/dsp_server"
	"gitlab.com/rapidcodelab-opensource/smartyads-native-wrapper/pkg/logging"
)

func main() {
	l := logging.Getlogger()
	s := dspserver.New(&l)
	s.Run()
}
