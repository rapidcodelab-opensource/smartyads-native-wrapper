# Fake DSP

Fake OpenRTB 2.5 Demand Side Platform (DSP) for Banner/Native BidRequests.

Useful for development with OpenRTB technologies. Provides the endpoints to test OpenRTB Banner/Native Ad BidRequests for your software.

Spec: https://www.iab.com/wp-content/uploads/2016/03/OpenRTB-API-Specification-Version-2-5-FINAL.pdf


### OpenRTB 2.5 BidReqest for Banner Ad. (Required and Recommeded Fields Only)

```json
{
    "id":"2776948d-0fba-4efb-b5c1-f860c9a115bd",
    "imp":[
        {
            "id":"1",
            "banner":{
                "format":[
                    {
                        "w": 250,
                        "h": 250
                    }
                ]
            }
        }
    ],
    "site":{
        "id":"2768"
    },
    "device":{
        "ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15",
        "geo":{
            "country":"USA"
        },
        "dnt":0,
        "lmt":0,
        "ip": "24.124.12.134"
    },
    "user":{
        "id":"840385f2-52cb-4411-a9ff-1fba488343fe"
    }
}
```

### OpenRTB 2.5 BidResponse for Banner Ad. (Required and Recommeded Fields Only)

```json
{
    "id":"2776948d-0fba-4efb-b5c1-f860c9a115bd",
    "seatbid":[
        {
            "bid":[
                {
                    "id":"4c05a791-3ff4-4868-a49d-c1b355f339ba",
                    "impidid":"1",
                    "price": 0.045,
                    "adm":"<img src=\"//track.rapidcodelab.com/images/{AUCTION_PRICE}/{SESSION_ID}/banner.gif\"/>"
                }
            ]
        }
    ]
}
```

### OpenRTB 2.5 & Dynamic Native Ads 1.2 BidReqest for Native Ad. (Required and Recommeded Fields Only) 

```json
{
    "id":"2776948d-0fba-4efb-b5c1-f860c9a115bd",
    "imp":[
        {
            "id":"1",
            "native":{
                "request":"JSON-encoded string of Native Markup Request Object",
                "ver":"1.2"
            }
        }
    ],
    "site":{
        "id":"2768"
    },
    "device":{
        "ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15",
        "geo":{
            "country":"USA"
        },
        "dnt":0,
        "lmt":0,
        "ip": "24.124.12.134"
    },
    "user":{
        "id":"840385f2-52cb-4411-a9ff-1fba488343fe"
    }
}
```

### Native Markup Request Object

The sample Native Request asking Native Ad Object with three required asstes: Title, Image(Main), Data(Description)

```json
{
    "ver":"1.2",
    "context": 1,
    "plcmttype": 1, 
    "assets":[
        {
            "id": 1,
            "required": 1, 
            "title": {
                "len": 25
            }
        },
        {
            "id": 2,
            "required": 1,
            "img":{
                "type": 3,
                "wmin": 200,
                "hmin": 200
            }
        },
        {
            "id": 3,
            "required": 1,
            "data":{
                "type": 2
            }
        }

    ]
}
```


### OpenRTB 2.5 & Dynamic Native Ads 1.2 BidReponse for Native Ad. (Required and Recommeded Fields Only) 


```json
{
    "id":"2776948d-0fba-4efb-b5c1-f860c9a115bd",
    "seatbid":[
        {
            "bid":[
                {
                    "id":"4c05a791-3ff4-4868-a49d-c1b355f339ba",
                    "impidid":"1",
                    "price": 0.045,
                    "adm":"JSON-encoded string of Native Markup Response Object"
                }
            ]
        }
    ]
}
```

### Native Markup Response Object

```json
{
    "ver": "1.2",
    "link": {
        "url": "//target.com"
    },
    "assets":[
        {
            "id": 1,
            "required": 1,
            "title":{
                "text": "30 Reasons Why You're Better OF Single"
            }
        },
        {
            "id": 2,
            "required": 1,
            "img":{
                "url": "//target.com/banner.jpg"
            }
        },
        {
            "id": 3,
            "required": 1,
            "data":{
                "value": "1. They just opened a Cic'c Buffet within ..."
            }
        }
    ]
}
```