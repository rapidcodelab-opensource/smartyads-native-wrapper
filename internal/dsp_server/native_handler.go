package dspserver

import (
	"encoding/json"
	"sync"
	"time"

	"github.com/mxmCherry/openrtb/v15/openrtb2"
	"github.com/valyala/fasthttp"
	rtbvalidators "gitlab.com/rapidcodelab-opensource/smartyads-native-wrapper/pkg/rtb_validators"
)

func (s *server) nativeHandler(ctx *fasthttp.RequestCtx) {
	var err error

	bidRequest := openrtb2.BidRequest{}
	err = json.Unmarshal(ctx.Request.Body(), &bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	err = rtbvalidators.ValidateOpenRTB25NativeBidRequest(&bidRequest)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBody([]byte(err.Error()))
		return
	}

	bids := []openrtb2.Bid{}

	wg := sync.WaitGroup{}
	wg.Add(len(bidRequest.Imp))

	for i := 0; i < len(bidRequest.Imp); i++ {

		cuttedBidRequest := bidRequest

		cuttedBidRequest.Imp = cuttedBidRequest.Imp[i : i+1]

		go func(wgx *sync.WaitGroup,
			bidRequest openrtb2.BidRequest,
			bids *[]openrtb2.Bid,
			impressionID string) {

			defer wgx.Done()

			req := fasthttp.AcquireRequest()
			res := fasthttp.AcquireResponse()

			defer fasthttp.ReleaseRequest(req)
			defer fasthttp.ReleaseResponse(res)

			req.SetRequestURI(s.config.EndpointURL)

			bidRequest.Imp[0].Instl = 0

			isSecure := int8(1)

			bidRequest.Imp[0].Secure = &isSecure

			bidRequestJSON, err := json.Marshal(bidRequest)
			if err != nil {
				s.logger.Error(err)
				return
			}

			s.logger.Warn("Request: ", string(bidRequestJSON))
			req.SetBody(bidRequestJSON)
			req.Header.SetMethod(fasthttp.MethodPost)

			err = s.httpClient.DoTimeout(req, res, 1000*time.Millisecond)
			if err != nil {
				s.logger.Error(err)
				return
			}

			bidResposne := openrtb2.BidResponse{}

			if res.StatusCode() != fasthttp.StatusOK {
				s.logger.Warn(res.StatusCode())
				return
			}

			err = json.Unmarshal(res.Body(), &bidResposne)
			if err != nil {
				s.logger.Error(err)
				return
			}

			bid := bidResposne.SeatBid[0].Bid[0]

			bid.ImpID = impressionID

			*bids = append(*bids, bid)
		}(&wg,
			cuttedBidRequest,
			&bids,
			bidRequest.Imp[i].ID)

	}

	wg.Wait()

	if len(bids) < 1 {
		ctx.Response.SetStatusCode(fasthttp.StatusNoContent)
		return
	}

	bidResponse := openrtb2.BidResponse{}

	bidResponse.ID = bidRequest.ID

	seatBid := openrtb2.SeatBid{
		Bid: bids,
	}

	bidResponse.SeatBid = append(bidResponse.SeatBid, seatBid)

	bidResponseJSON, err := json.Marshal(bidResponse)
	if err != nil {
		s.logger.Warn(err)
		ctx.Response.SetStatusCode(fasthttp.StatusBadGateway)
		return
	}

	//send response
	ctx.Response.Header.SetContentType("application/json")
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
	ctx.Response.SetBody(bidResponseJSON)

}
